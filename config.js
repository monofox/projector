import Icon from './img/projector.svg';

export default {
	id: "projector",
	meta: {
		title: 	{
			"de": "Online Projektor",
			"en": "Projection Screen"
		},
		text:	{
			"de": "Projizieren von Bildern, die Teilnehmer online senden",
			"en": "Project images that participants send online"
		},
		to: "projector-index",
		icon: Icon,
		role: "projector",
		index:true
	},
	routes: [
		{	path: '/projector-index', name:'projector-index', component: () => import('./views/Index.vue') },
		{	path: '/projector/:ticket', name:'projector-part-ticket', component: () => import('./views/Part.vue') },
		{	path: '/projector', name:'projector-part', component: () => import('./views/Part.vue') },
	],
	imageMaxSize:1024000
}
