//External libs
const { check,oneOf,validationResult }=require('express-validator/check');
const { sanitize }=require('express-validator/filter');
const config=__config;
const consts=require ('../../../consts.js');
const debug=require('debug')('projector');
const cron=require('node-cron');
const path=require('path');
const generator=require('../../../main/server/passwordHelper');
const WebSocket=require('ws');

//Internal libs
const mainController=require('../../../main/server/controller');
const userHelper=require('../../../main/server/userHelper');

//Constants
const TICKET_LENGTH=10;
const WS_PING_INTERVAL=100;

/********************************************************************************
 Common functions
 ********************************************************************************/

/*
 Check permission to use the tool
  - Use see route
 */
exports.checkAuth=function(req, res, next) {
	mainController.checkAuthAndRole(req,res,next,"projector");
}


/********************************************************************************
 Functions for author
 ********************************************************************************/

 /*
  Get meta infos of all chats
  - Authentication via checkAuth (route)
  - Authorization with DB request (session.login)
  */
 exports.getProjector=function(req, res, next) {
     var ticket=generator.generate({length: TICKET_LENGTH, numbers: true});
     var url=(config.URL_PROTO=='http://' ? 'ws://' : 'wss://')+path.join(config.URL_HOST,config.URL_PREFIX,'projector','ws',ticket);
     res.json({ticket:ticket,url:url});
 }


/********************************************************************************
Normal participants
********************************************************************************/

//initially check ticket and availability, used in routes
exports.checkTicket=[
 	check('ticket').trim().matches(consts.REGEX_TICKET).withMessage('ticket-invalid'),
 	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
        var ticket=req.params.ticket || req.query.ticket;
		if (!ticket) return res.json([{msg:'validation-error'}]);
        if (!webSockets[ticket]) {
            return res.json([{'msg':'ticket-invalid'}])
        }
        res.locals.ticket=ticket;
        next();
	}
]

//return ticket - used in order to check ticket for client
exports.returnTicket=function(req,res,next) {
    res.json({ticket:res.locals.ticket});
}

//Send message
exports.sendMessage=function(req, res, next)  {
	var image='';
	//if (req.body.image && req.body.image.match(consts.REGEX_DATA_URI)) {
    if (req.body.image && req.body.image.indexOf('data:image')==0) {
		image=req.body.image;
        sendImage(res.locals.ticket,image);
        return res.json({});
	}
    return res.json([{'msg':'unknown-error'}]);
}




/********************************************************************************
 Websocket
 ********************************************************************************/

//List of all websocket clients
var webSockets={};

/*
 Register new client
*/
exports.initWs=function(ws, req) {
	var ticket=req.params.ticket;
	if (!ticket || !ticket.match(consts.REGEX_TICKET)) {
        debug('Ignored WS request with invalid ticket.');
        return;
    }
    webSockets[ticket]=ws;
	ws.on('pong', function() {
		this.isAlive=true;
	});
	debug("New WS projector client for ticket "+ticket);
}

/*
 Broadcast message
 */
function sendImage(ticket,image) {
	var socket=webSockets[ticket];
    var message=JSON.stringify({image:image});
	if (socket && socket.readyState==WebSocket.OPEN) {
        debug('Sending image to WS')
		try {socket.send(message)} catch (e) {debug(e);}
	}
}


/*
Send ping to all websockets
 - to check if they are still connected
 - Ensure that the reverse proxy does not break the connection
 */
setInterval(function () {
	var count=0;
	for ( ticket in webSockets) {
        var socket=webSockets[ticket];
		if (socket.isAlive===false || socket.readyState!=WebSocket.OPEN) {
			debug("WS projector client closed for ticket: "+ticket);
			delete webSockets[ticket];
			try {socket.close();} catch (e) {debug(e);}
		}
		else {
			count++;
			socket.isAlive=false;
			try { socket.ping();} catch (e) {debug (e);}
		}
	}
	debug("Number of open ws sockets: "+count)
}, WS_PING_INTERVAL*1000);



/********************************************************************************
 Utils
 ********************************************************************************/
